//
//  HomeViewController.swift
//  CareerFair
//
//  Created by Prashannajeet Prasann on 12/03/19.
//  Copyright © 2019 Prashannajeet Prasann. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    var registrationObject = RegistrationDetails()

    //Mark :- Life cycle method
    override func viewDidLoad() {
        
        var finalArrayOfRegistration = UserDefaults.standard.value(forKey: "registrationData") == nil ? [[String:String]]() : UserDefaults.standard.value(forKey: "registrationData") as! [[String:String]]
        
        var arrayOfRegistration = [[String:String]]()
        if finalArrayOfRegistration.count > 5 {
            arrayOfRegistration = Array(finalArrayOfRegistration[(finalArrayOfRegistration.count-5)..<finalArrayOfRegistration.count])
        } else {
            arrayOfRegistration = finalArrayOfRegistration
        }
        
        postDataToServer(registrationArray: arrayOfRegistration, finalArray: finalArrayOfRegistration)
    }
    func postDataToServer(registrationArray : [[String:String]], finalArray :[[String:String]]) {
         var finalArrayOfRegistration = finalArray
        var arrayOfRegistration = registrationArray
        
        if arrayOfRegistration.count > 0 {
            DispatchQueue.global(qos: .background).async {
                print("This is run on the background queue")
                if Reachability.isConnectedToNetwork() {
                    
                    let finalDictionary = ["Registrations": arrayOfRegistration]
                    
                    
                    let Url = String(format: "http://sercocareerfair.azurewebsites.net/Api/Registration")
                    guard let serviceUrl = URL(string: Url) else { return }
                    var request = URLRequest(url: serviceUrl)
                    request.httpMethod = "POST"
                    request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
                    guard let httpBody = try? JSONSerialization.data(withJSONObject: finalDictionary, options: []) else {
                        return
                    }
                    request.httpBody = httpBody
                    let session = URLSession.shared
                    session.dataTask(with: request) { (data, response, error) in
                        if let response = response {
                            print(response)
                        }
                        if let data = data {
                            do {
                                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                                
                                DispatchQueue.main.async { [unowned self] in
                                    self.hideHUD()
                                    let refinedResult = json?["oResponseList"] as! [[String:Any]]
                                    let response = refinedResult[0]["Code"] as! String
                                    
                                    if response == "0" || response == "-2" {
                                        
                                        if finalArrayOfRegistration.count > 5 {
                                            
                                            for objRegistration in refinedResult {
                                                let filteredArray = finalArrayOfRegistration.filter{$0["RequestID"] == objRegistration["RequestID"] as? String}
                                                
                                                if filteredArray.count > 0 {
                                                    let index = finalArrayOfRegistration.index(where: {$0 == filteredArray[0]})
                                                    finalArrayOfRegistration.remove(at: index!)
                                                }
                                            }
                                            
                                            if finalArrayOfRegistration.count > 5 {
                                                arrayOfRegistration = Array(finalArrayOfRegistration[(finalArrayOfRegistration.count-5)..<finalArrayOfRegistration.count])
                                                
                                            } else {
                                                arrayOfRegistration = finalArrayOfRegistration
                                            }
                                            
                                            self.postDataToServer(registrationArray: arrayOfRegistration, finalArray: finalArrayOfRegistration)
                                        }
                                        else {
                                            UserDefaults.standard.removeObject(forKey: "registrationData")
                                        }
                                    }
                                }
                            } catch {
                                print("Error Serializing JSON: \(error)")
                            }
                        }
                        }.resume()
                }
            }
        }
    }
    
    @IBAction func startButtonTapped(_ sender: Any) {
        let vc = PersonalDetailViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension UIViewController {
    
    func showHUD(progressLabel:String){
        let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        progressHUD?.labelText = progressLabel
    }
    func showHud(){
        _ = MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func showAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { action in
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showConfirmationAlert(alertTitle:String,alertMessage:String,actionsArray:[UIAlertAction])
    {
        self.view.endEditing(true)
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        for action in actionsArray
        {
            alertController.addAction(action)
        }
        self.present(alertController, animated: true, completion: nil)
    }
}
