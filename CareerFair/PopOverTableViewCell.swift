//
//  PopOverTableViewCell.swift
//  CareerFair
//
//  Created by Prashannajeet Prasann on 13/03/19.
//  Copyright © 2019 Prashannajeet Prasann. All rights reserved.
//

import UIKit

class PopOverTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
