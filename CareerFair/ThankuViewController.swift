//
//  ThankuViewController.swift
//  CareerFair
//
//  Created by Prashannajeet Prasann on 16/03/19.
//  Copyright © 2019 Prashannajeet Prasann. All rights reserved.
//

import UIKit

class ThankuViewController: UIViewController {
    
    override func viewDidLoad() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
            // Put your code which should be executed with a delay here
            let vc = HomeViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        })
    }
}
