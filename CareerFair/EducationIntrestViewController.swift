//
//  EducationIntrestViewController.swift
//  CareerFair
//
//  Created by Prashannajeet Prasann on 14/03/19.
//  Copyright © 2019 Prashannajeet Prasann. All rights reserved.
//

import UIKit

class EducationIntrestViewController: UIViewController, CareerFairPopOverDelegate, UITextFieldDelegate{
    
    
    
    @IBOutlet var fullTimeButton: UIButton!
    @IBOutlet var partTimeButton: UIButton!
    
    var selectedJobType = ""
    var graduationYear :[String] = ["1990","1991", "1992", "1993", "1994", "1995", "1996","1997","1998", "1999", "2000", "2001", "2002", "2003","2004","2005", "2006", "2007", "2008", "2009", "2010","2011","2012", "2013", "2014", "2015", "2016", "2017","2018","2019"]
    var jobLocationArray :[String] = ["Abu Dhabi","Dubai", "Sharjah", "Ajman", "Umm Al Quwwain", "Ras Al Khaimah", "Fujairah"]
    
    var yearsOfExp :[String] = ["0","1", "2", "4", "5", "6", "7","8","9", "10", "11", "12", "13", "14","15+"]
    var areasOfInterest : [String] = ["Administration", "Customer Service","Engineering", "Finance", "Government Relations", "Human Resources", "Information Technology", "Legal", "Marketing", "Procurement", "Sales"]
    var selectedTitle = ""
    open var popoverSize = CGSize(width: 500, height: 500)
    var gradientView:UIView = UIView()
    var educationalLevelString : [String] = ["Secondary", "High School", "Bachelors Degree", "Masters Degree", "Other"]
    
    @IBOutlet var educationalLevelTextField: UITextField!
    @IBOutlet var majorTextField: UITextField!
    @IBOutlet var graduationYearTextField: UITextField!
    @IBOutlet var yearsOfExpTextField: UITextField!
    @IBOutlet var areaOfInterestTextField: UITextField!
    @IBOutlet var otherTextField: UITextField!
    var registrationObject = RegistrationDetails()
    
    @IBOutlet var number1Label: UILabel!
    @IBOutlet var number2Label: UILabel!
    @IBOutlet var number3Label: UILabel!
    @IBOutlet var number4Label: UILabel!
    
    
    
    var domain = String()
    var domain1 = String()
    var domain2 = String()
    var timestamp = String()
    var randomNumber = String()
    var guid = String()
    var deviceID = String()
    
    override func viewDidLoad() {
        
        number1Label.layer.cornerRadius = 15
        number1Label.layer.masksToBounds = true
        number2Label.layer.cornerRadius = 15
        number2Label.layer.masksToBounds = true
        number3Label.layer.cornerRadius = 15
        number3Label.layer.masksToBounds = true
        number4Label.layer.cornerRadius = 15
        number4Label.layer.masksToBounds = true
        otherTextField.text = ""
        majorTextField.text = ""
        
    }
    @IBAction func fullTimeButtonTapped(_ sender: Any) {
        selectedJobType = "Full Time"
        fullTimeButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        partTimeButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
    }
    @IBAction func partTimeButtonTapped(_ sender: Any) {
        selectedJobType = "Part Time"
        partTimeButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        fullTimeButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
    }
    
    //MARK: - UITextField Methods
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == educationalLevelTextField
        {
            self.view .endEditing(true)
            self.selectedTitle = "Educational Level"
            let popoverVC = PopOverViewController()
            popoverVC.delegate = self
            popoverVC.stringsArray = self.educationalLevelString
            popoverVC.delegate=self
            popoverVC.titleText = self.selectedTitle
            popoverVC.currentPopOverSize=self.popoverSize
            let visitNavController = UINavigationController(rootViewController: popoverVC)
            visitNavController.preferredContentSize = self.popoverSize
            visitNavController.modalPresentationStyle = .formSheet
            visitNavController.navigationBar.isHidden = true
            self.showGradiedntViewForPopOver()
            self.present(visitNavController, animated: true, completion: nil)
        }
        
        if textField == graduationYearTextField
        {
            self.view .endEditing(true)
            selectedTitle = "Graduation Year"
            let popoverVC = PopOverViewController()
            popoverVC.delegate = self
            popoverVC.stringsArray = graduationYear
            popoverVC.delegate=self
            popoverVC.titleText = selectedTitle
            popoverVC.currentPopOverSize=self.popoverSize
            let visitNavController = UINavigationController(rootViewController: popoverVC)
            visitNavController.preferredContentSize = self.popoverSize
            visitNavController.modalPresentationStyle = .formSheet
            visitNavController.navigationBar.isHidden = true
            self.showGradiedntViewForPopOver()
            self.present(visitNavController, animated: true, completion: nil)
        }
        if textField == yearsOfExpTextField {
            self.view .endEditing(true)
            selectedTitle = "Years of Experience"
            let popoverVC = PopOverViewController()
            popoverVC.delegate = self
            popoverVC.stringsArray = yearsOfExp
            popoverVC.delegate=self
            popoverVC.titleText = selectedTitle
            popoverVC.currentPopOverSize=self.popoverSize
            let visitNavController = UINavigationController(rootViewController: popoverVC)
            visitNavController.preferredContentSize = self.popoverSize
            visitNavController.modalPresentationStyle = .formSheet
            visitNavController.navigationBar.isHidden = true
            self.showGradiedntViewForPopOver()
            self.present(visitNavController, animated: true, completion: nil)
        }
        if textField == areaOfInterestTextField{
            self.view .endEditing(true)
            selectedTitle = "Area of Interest"
            let popoverVC = PopOverViewController()
            popoverVC.delegate = self
            popoverVC.stringsArray = areasOfInterest
            popoverVC.delegate=self
            popoverVC.titleText = selectedTitle
            popoverVC.currentPopOverSize=self.popoverSize
            let visitNavController = UINavigationController(rootViewController: popoverVC)
            visitNavController.preferredContentSize = self.popoverSize
            visitNavController.modalPresentationStyle = .formSheet
            visitNavController.navigationBar.isHidden = true
            self.showGradiedntViewForPopOver()
            self.present(visitNavController, animated: true, completion: nil)
        }
        if textField == otherTextField {
            return true
        }
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) { // became first responder
        
        if textField == otherTextField {
            animateViewMoving(up: true, moveValue: 300)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == otherTextField {
            animateViewMoving(up: false, moveValue: 300)
        }
    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    func didCancelPopOver() {
        hideGradientViewForPopOver()
        
    }
    
    func didSelectStringinPopOver(selectedString: String) {
        if selectedTitle == "Educational Level" {
            educationalLevelTextField.text = selectedString
        }
        if selectedTitle == "Graduation Year"{
            graduationYearTextField.text = selectedString
        }
        if selectedTitle == "Years of Experience" {
            yearsOfExpTextField.text = selectedString
        }
        if selectedTitle == "Area of Interest" {
            areaOfInterestTextField.text = selectedString
        }
        hideGradientViewForPopOver()
        
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if educationalLevelTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please select educational level")
        }else if graduationYearTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please select graduation year")
        }else if yearsOfExpTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please select years of experience")
        }else if areaOfInterestTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please select area of interest")
        }else if selectedJobType == ""{
            showAlert(withTitle: "", message: "Please select preferred job type")
            
        }
        else {
            self.showHud()
            let areaOfInterestString = areaOfInterestTextField.text
            let areaOfInterestArray = areaOfInterestString!.components(separatedBy: ",")
            
            if areaOfInterestArray.count == 1 {
                domain = areaOfInterestArray[0]
            }
            if areaOfInterestArray.count == 2 {
                domain = areaOfInterestArray[0]
                domain1 = areaOfInterestArray[1]
            }
            if areaOfInterestArray.count == 3 {
                domain = areaOfInterestArray[0]
                domain1 = areaOfInterestArray[1]
                domain2 = areaOfInterestArray[2]
            }

            let dob = convertDateFormater(registrationObject.DOB)

            
            registrationObject.Educational_Level = educationalLevelTextField.text!
            registrationObject.Major = majorTextField.text!
            registrationObject.Graduation_Year = graduationYearTextField.text!
            registrationObject.Years_Of_Exp = yearsOfExpTextField.text!
            registrationObject.Domain = domain
            registrationObject.Domain1 = domain1
            registrationObject.Domain2 = domain2

            registrationObject.Other = otherTextField.text!
            registrationObject.Preferred_Job_Type = selectedJobType
            
            timestamp = SercoDefaults.retrieveDefaults.fetchCurrentTimestamp()
            randomNumber = SercoDefaults.retrieveDefaults.randomString(length: 10).uppercased()
            guid = SercoDefaults.retrieveDefaults.createGuid()
            deviceID = UIDevice.current.identifierForVendor!.uuidString
            
            
            var arrayOfRegistration = UserDefaults.standard.value(forKey: "registrationData") == nil ? [[String:String]]() : UserDefaults.standard.value(forKey: "registrationData") as! [[String:String]]
            
            let parametersDictionary : [String:String] = ["AppID":SercoDefaults.retrieveDefaults.kAppKey,"ApplicantID":randomNumber,"RequestID":guid,"DeviceID":deviceID,"FirstName":registrationObject.First_Name,"LastName":registrationObject.Last_Name,"BirthDate":dob,"Gender":registrationObject.Gender,"Nationality":registrationObject.Nationality, "Emirate":registrationObject.Emirates, "NationalID":"971","UAENationalFamilyBook":registrationObject.Kholasat_Al_Qaid,"Email":registrationObject.Email_Address,  "MobileNumber":registrationObject.Mobile_Number, "Language1":registrationObject.Language1, "Language2":registrationObject.Language2, "Language3":registrationObject.Language3, "EducationalLevel":registrationObject.Educational_Level, "Major":registrationObject.Major, "GraduationYear":registrationObject.Graduation_Year, "YearsOfExperience":registrationObject.Years_Of_Exp, "DomainArea":domain, "DomainArea1":domain1, "DomainAreaOther":registrationObject.Other, "DomainArea2":domain2, "JobLocation":"", "JobType":registrationObject.Preferred_Job_Type, "WorkShift":registrationObject.ShiftBasis, "WearUniform":registrationObject.WearUniform, "EducationalLevelOthers":"", "RegisteredAt":timestamp, "PostedAt":timestamp]
            
            arrayOfRegistration.append(parametersDictionary)
            postDataToServer(arrayOfRegistration: arrayOfRegistration)
        }
    }
    func convertDateFormater(_ date: String) -> String
    {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd-MM-yyyy"
        let showDate = inputFormatter.date(from: date)
        inputFormatter.dateFormat = "yyyyMMdd"
        let resultString = inputFormatter.string(from: showDate!)
        return resultString
        
    }
    func postDataToServer(arrayOfRegistration :[[String:String]]){
        if Reachability.isConnectedToNetwork() {
            let finalDictionary = ["Registrations": arrayOfRegistration]
            
            
            let Url = String(format: "http://sercocareerfair.azurewebsites.net/Api/Registration")
            guard let serviceUrl = URL(string: Url) else { return }
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            guard let httpBody = try? JSONSerialization.data(withJSONObject: finalDictionary, options: []) else {
                return
            }
            request.httpBody = httpBody
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                if let response = response {
                    print(response)
                }
                if let data = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                        
                        DispatchQueue.main.async { [unowned self] in
                            self.hideHUD()
                            
                            let refinedResult = json["oResponseList"] as! [[String:AnyObject]]
                            let response = refinedResult[0]["Code"] as! String
                            let failedMessage = json["Message"] as! String
                            
                            if response == "0" || response == "-2" {
                                
                                UserDefaults.standard.removeObject(forKey: "registrationData")
                                let vc = ThankuViewController()
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }else{
                                let okAction = UIAlertAction(title: "OK", style: .default) { action in
                                }
                                self.showConfirmationAlert(alertTitle: "Failed", alertMessage: failedMessage, actionsArray: [okAction])
                            }
                        }
                    } catch {
                        print("Error Serializing JSON: \(error)")
                    }
                }
                }.resume()
            
        }else{
            self.hideHUD()
            UserDefaults.standard.set(arrayOfRegistration, forKey: "registrationData")
            let vc = ThankuViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func showGradiedntViewForPopOver()
    {
        gradientView = UIView(frame: (self.navigationController?.view.bounds)!)
        gradientView.backgroundColor = UIColor(red: 44.0/255.0, green: 57.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        gradientView.layer.opacity = 0.5
        self.navigationController?.view.addSubview(gradientView)
    }
    func hideGradientViewForPopOver()  {
        gradientView.removeFromSuperview()
    }
}
