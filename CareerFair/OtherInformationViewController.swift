//
//  OtherInformationViewController.swift
//  CareerFair
//
//  Created by Prashannajeet Prasann on 14/03/19.
//  Copyright © 2019 Prashannajeet Prasann. All rights reserved.
//

import UIKit


class OtherInformationViewController: UIViewController, UITextFieldDelegate, CareerFairPopOverDelegate{
    func didCancelPopOver() {
        hideGradientViewForPopOver()
    }
    
    func didSelectStringinPopOver(selectedString: String) {
        if  selectedTitle == "Languages"{
            languageTextField.text = selectedString
        }else{
            emirateTextField.text = selectedString
        }
        hideGradientViewForPopOver()
    }
    
    
    var registrationObject = RegistrationDetails()

    
    @IBOutlet var emailAddressTextField: UITextField!
    @IBOutlet var mobileNumberTextField: UITextField!
    @IBOutlet var emirateTextField: UITextField!
    @IBOutlet var languageTextField: UITextField!
    
    var selectedShiftBasis = ""
    var selectedUniform = ""
    
    @IBOutlet var shiftBasisYesButton: UIButton!
    @IBOutlet var shiftBasisNoButton: UIButton!

    @IBOutlet var mobileNumberView: UIView!
    @IBOutlet var uniformYesButton: UIButton!
    @IBOutlet var uniformNoButton: UIButton!
    var emirateArray :[String] = ["Abu Dhabi","Dubai", "Sharjah", "Ajman", "Umm Al Quwwain", "Ras Al Khaimah", "Fujairah"]
    
    var languageArray :[String] = ["Arabic   عربى","English   الإنجليزية","Hindi   الهندية", "Spanish   الأسبانية", "French   الفرنسية", "Turkish   اللغة التركية", "Chinese   صينى", "German   الألمانية", "Italian   الإيطالي", "Russian   الروسية"]
    var selectedTitle = ""
    open var popoverSize = CGSize(width: 500, height: 500)
    var gradientView:UIView = UIView()
    
    @IBOutlet var number1Label: UILabel!
    @IBOutlet var number2Label: UILabel!
    @IBOutlet var number3Label: UILabel!
    @IBOutlet var number4Label: UILabel!
    
    var language1 = String()
    var language2 = String()
    var language3 = String()
    
    override func viewDidLoad() {
        
        mobileNumberView.layer.cornerRadius = 4.0
        mobileNumberView.layer.masksToBounds = true
        
        number1Label.layer.cornerRadius = 15
        number1Label.layer.masksToBounds = true
        number2Label.layer.cornerRadius = 15
        number2Label.layer.masksToBounds = true
        number3Label.layer.cornerRadius = 15
        number3Label.layer.masksToBounds = true
        number4Label.layer.cornerRadius = 15
        number4Label.layer.masksToBounds = true
        
        
        emailAddressTextField.text = registrationObject.Email_Address
        mobileNumberTextField.text = registrationObject.Mobile_Number
        emirateTextField.text = registrationObject.Emirates
        let language = registrationObject.Language1 + "," + registrationObject.Language2 + "," + registrationObject.Language3
        
        languageTextField.text = language
        
        if registrationObject.ShiftBasis == "Yes"{
            shiftBasisYesButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
            shiftBasisNoButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        }else if registrationObject.ShiftBasis == "No"{
            shiftBasisNoButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
            shiftBasisYesButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        }
        
        if registrationObject.WearUniform == "Yes" {
            uniformYesButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
            uniformNoButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        }else if registrationObject.WearUniform == "No"{
            uniformNoButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
            uniformYesButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        }
    }
    
    @IBAction func shiftBasisYesButtonTapped(_ sender: Any) {
        selectedShiftBasis = "Yes"
        shiftBasisYesButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        shiftBasisNoButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
    }
    @IBAction func shiftBasisNoButtonTapped(_ sender: Any) {
        selectedShiftBasis = "No"
        shiftBasisNoButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        shiftBasisYesButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
    }
    
    //MARK: - UITextField Methods
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == emirateTextField
        {
            self.view .endEditing(true)
            selectedTitle = "Emirate"
            let popoverVC = PopOverViewController()
            popoverVC.delegate = self
            popoverVC.stringsArray = emirateArray
            popoverVC.delegate=self
            popoverVC.titleText = selectedTitle
            popoverVC.currentPopOverSize=self.popoverSize
            let visitNavController = UINavigationController(rootViewController: popoverVC)
            visitNavController.preferredContentSize = self.popoverSize
            visitNavController.modalPresentationStyle = .formSheet
            visitNavController.navigationBar.isHidden = true
            self.showGradiedntViewForPopOver()
            self.present(visitNavController, animated: true, completion: nil)
        }
        if textField == languageTextField {
            self.view .endEditing(true)
            selectedTitle = "Languages"
            let popoverVC = PopOverViewController()
            popoverVC.delegate = self
            popoverVC.stringsArray = languageArray
            popoverVC.delegate=self
            popoverVC.titleText = selectedTitle
            popoverVC.currentPopOverSize=self.popoverSize
            let visitNavController = UINavigationController(rootViewController: popoverVC)
            visitNavController.preferredContentSize = self.popoverSize
            visitNavController.modalPresentationStyle = .formSheet
            visitNavController.navigationBar.isHidden = true
            self.showGradiedntViewForPopOver()
            self.present(visitNavController, animated: true, completion: nil)
        }
        if textField == mobileNumberTextField{
            return true
        }
        
        return false
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == mobileNumberTextField {
            if string.rangeOfCharacter(from: SercoDefaults.retrieveDefaults.kAcceptableCharactersForMobileNumberinTextField) != nil
            {
                return false
            }
            else{
                return true
            }
        }else{
            return true
        }
    }
    
    @IBAction func uniformYesButtonTapped(_ sender: Any) {
        uniformYesButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        uniformNoButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        selectedUniform = "Yes"
    }
    
    @IBAction func uniformNoButtonTapped(_ sender: Any) {
        uniformNoButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        uniformYesButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        selectedUniform = "No"
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if emailAddressTextField.text?.count == 0 {
            showAlert(withTitle: "", message: "Please enter your email address")
        }
        else if !isValidEmail(testStr: emailAddressTextField.text!) {
            showAlert(withTitle: "", message: "Please enter a valid email address")
        }
        else if mobileNumberTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please enter your mobile number")
        }
        else if (((mobileNumberTextField.text?.count)!) > 9 || ((mobileNumberTextField.text?.count)!) < 9){
            showAlert(withTitle: "", message: "Mobile number should be of 9 digit only")
        } else if !((mobileNumberTextField.text?.starts(with: "5"))!){
            showAlert(withTitle: "", message: "Mobile number should start with 5 only")
            
        }else if emirateTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please select emirate")
        }else if languageTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please select a Language")
        }else if selectedShiftBasis == ""{
            showAlert(withTitle: "", message: "Please select are you willing to work on a shift basis?")
        }else if selectedUniform == ""{
            showAlert(withTitle: "", message: "Please select are you willing to wear a uniform?")
        }
        else{
            
            let languageString = languageTextField.text!
            let languageArray = languageString.components(separatedBy: ",")
            
            if languageArray.count == 1 {
                language1 = languageArray[0]
            }
            if languageArray.count == 2 {
                language1 = languageArray[0]
                language2 = languageArray[1]
            }
            if languageArray.count == 3 {
                language1 = languageArray[0]
                language2 = languageArray[1]
                language3 = languageArray[2]
            }
            
            registrationObject.Email_Address = emailAddressTextField.text!
            registrationObject.Mobile_Number = mobileNumberTextField.text!
            registrationObject.Emirates = emirateTextField.text!
            registrationObject.Language1 = language1
            registrationObject.Language2 = language2
            registrationObject.Language3 = language3
            registrationObject.ShiftBasis = selectedShiftBasis
            registrationObject.WearUniform = selectedUniform
            
            let vc = EducationIntrestViewController()
            vc.registrationObject = self.registrationObject
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func showGradiedntViewForPopOver()
    {
        gradientView = UIView(frame: (self.navigationController?.view.bounds)!)
        gradientView.backgroundColor = UIColor(red: 44.0/255.0, green: 57.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        gradientView.layer.opacity = 0.5
        self.navigationController?.view.addSubview(gradientView)
    }
    func hideGradientViewForPopOver()  {
        gradientView.removeFromSuperview()
    }
}

