//
//  PopOverViewController.swift
//  CareerFair
//
//  Created by Prashannajeet Prasann on 12/03/19.
//  Copyright © 2019 Prashannajeet Prasann. All rights reserved.
//

import UIKit

protocol CareerFairPopOverDelegate : class
{
    func didCancelPopOver()
    func didSelectStringinPopOver(selectedString:String)
}

class PopOverViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var contentTableView: UITableView!
    @IBOutlet var contentSearchBar: UISearchBar!
    @IBOutlet var titleTabel: UILabel!
    var stringsArray :[String] = []
    var unfilteredStringsArray :[String] = []

    @IBOutlet var doneButton: UIButton!
    weak var delegate : CareerFairPopOverDelegate?
    var titleText : String = ""
    var isSearching = false
    var currentPopOverSize : CGSize?
    var languageArray : [String] = []
    var areaOfInterestArray : [String] = []

    
    override func viewDidLoad() {
        
        super .viewDidLoad()
        contentSearchBar.delegate = self
        unfilteredStringsArray = stringsArray
        contentTableView.tableFooterView = UIView()
        
        contentTableView.register(UINib(nibName: "PopOverTableViewCell", bundle: nil), forCellReuseIdentifier: "defaultCell")
        contentTableView.estimatedRowHeight = 44
        contentTableView.rowHeight = UITableView.automaticDimension
        titleTabel.text = titleText
        if titleText == "Languages" || titleText == "Area of Interest"{
            doneButton.isHidden = false
            contentTableView.allowsMultipleSelection = true
        }else{
            doneButton.isHidden = true
            contentTableView.allowsMultipleSelection = false
        }
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        
        if titleText == "Languages"{
            var finalLanguageArray = [String]()
            for objLanguage in languageArray {
                let separateLanguageTempArray = objLanguage.components(separatedBy: " ")
                
                if separateLanguageTempArray.count > 0 {
                    finalLanguageArray.append(separateLanguageTempArray[0])
                }
            }
            let selectedlanguages = finalLanguageArray.joined(separator: ",")
            self.delegate?.didSelectStringinPopOver(selectedString: selectedlanguages)
            self.dismiss(animated: true, completion: nil)
        }
        else{
            var finalInterestArray = [String]()
            for objInterest in areaOfInterestArray {
                let separateInterestTempArray = objInterest.components(separatedBy: " ")
                
                if separateInterestTempArray.count > 0 {
                    finalInterestArray.append(separateInterestTempArray[0])
                }
            }
            let selectedInterest = finalInterestArray.joined(separator: ",")
            self.delegate?.didSelectStringinPopOver(selectedString: selectedInterest)
            self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        delegate?.didCancelPopOver()
        self.dismiss(animated: true, completion: nil)
    }

    
    // MARK: - TableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringsArray.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell : PopOverTableViewCell = tableView.dequeueReusableCell(withIdentifier: "defaultCell", for: indexPath) as! PopOverTableViewCell
        cell.titleLabel?.text = stringsArray[indexPath.row]
        cell.accessoryType = .none
        
        if titleText == "Languages" {
            if languageArray.contains(stringsArray[indexPath.row])
            {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        if titleText == "Area of Interest"{
            if areaOfInterestArray.contains(stringsArray[indexPath.row])
            {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        contentSearchBar.resignFirstResponder()
        
        let matchedString = stringsArray[indexPath.row]
        
        if titleText == "Languages" {
            if languageArray.contains(matchedString) {
                let index = languageArray.index(where: {$0 == matchedString})
                languageArray.remove(at: index!)
            } else {
                if languageArray.count == 3 {
                    let successAlert = UIAlertController(title: "", message: "You can select maximum 3 Languages", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (currentAction) in
                    })
                    successAlert.addAction(okAction)
                    self.present(successAlert, animated: true, completion: nil)
                } else {
                    languageArray.append(matchedString)
                }
            }
            contentTableView.reloadData()
        } else if titleText == "Area of Interest" {
            if areaOfInterestArray.contains(matchedString) {
                let index = areaOfInterestArray.index(where: {$0 == matchedString})
                areaOfInterestArray.remove(at: index!)
            } else {
                if areaOfInterestArray.count == 3 {
                    let successAlert = UIAlertController(title: "", message: "You can select maximum 3 Areas", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (currentAction) in
                    })
                    successAlert.addAction(okAction)
                    self.present(successAlert, animated: true, completion: nil)
                } else {
                    areaOfInterestArray.append(matchedString)
                }
            }
            contentTableView.reloadData()
        } else {
            self.delegate?.didSelectStringinPopOver(selectedString: matchedString)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - UISearchBar
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        isSearching = true
        contentSearchBar.setShowsCancelButton(true, animated: true)
        self.searchContentwithText(searchString: searchText)
    }
    
    func searchContentwithText(searchString:String)
    {
        if searchString == "" {
            stringsArray = unfilteredStringsArray
            contentTableView.reloadData()
        }else{
        let filterContent = unfilteredStringsArray.filter({$0.lowercased().contains("\(searchString.lowercased())")})
        stringsArray = filterContent
        contentTableView.reloadData()
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        stringsArray = unfilteredStringsArray
        contentSearchBar.resignFirstResponder()
        contentSearchBar.setShowsCancelButton(false, animated: true)
        contentSearchBar.text = ""
        contentTableView.reloadData()
        isSearching = false
        
    }
    
}


