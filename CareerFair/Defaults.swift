//
//  Defaults.swift
//  OCR
//
//  Created by Unique Computer Systems on 2/16/19.
//  Copyright © 2019 Unique Computer Systems. All rights reserved.
//

import Foundation
import CommonCrypto

import SystemConfiguration
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

class SercoDefaults{

    static let retrieveDefaults = SercoDefaults()
    private init(){}

    let kAppKey :String = "426a416e4e6a703150443450436b34504868344641416f4948514147483145344e513d3d"
    let kAcceptableCharactersForMobileNumberinTextField = NSCharacterSet.init(charactersIn: "0123456789").inverted

    func fetchCurrentTimestamp() -> String {
        let currentDate = Date()
        let timeZone = NSTimeZone(name: "UTC")
        // or specifc Timezone: with name
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en-US")
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        if let timeZone = timeZone {
            dateFormatter.timeZone = timeZone as TimeZone
        }
        let DateString = dateFormatter.string(from: currentDate)
        return DateString
    }
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func createGuid() -> String {
        let uuidRef = CFUUIDCreate(nil)
        let uuidStringRef = CFUUIDCreateString(nil, uuidRef)
        let uuid = uuidStringRef! as String
        return uuid
    }
}

class RegistrationDetails
{
    var First_Name = String()
    var Last_Name = String()
    var Nationality = String()
    var DOB = String()
    var Kholasat_Al_Qaid = String()
    var Gender = String()
    var Email_Address = String()
    var Mobile_Number = String()
    var Emirates = String()
    var Language1 = String()
    var Language2 = String()
    var Language3 = String()
    var Educational_Level = String()
    var Major = String()
    var Graduation_Year = String()
    var Years_Of_Exp = String()
    var Domain = String()
    var Domain1 = String()
    var Domain2 = String()
    var Other = String()
    var Preferred_Job_Location = String()
    var Preferred_Job_Type = String()
    var ShiftBasis = String()
    var WearUniform = String()
    var Timestamp = String()
    var RandomNumber = String()
    var Guid = String()
    var DeviceID = String()
    
    
    convenience init () {
        self.init(firstName:"", lastName:"", nationality:"", dob:"", kholasatAlQaid:"", gender:"", emailAddress:"", mobileNumber:"", emirates:"", language1:"", language2:"", language3:"", educationalLevel:"", major:"", graduationYear:"", yearsOfExp:"", domain:"",domain1:"",domain2:"", other:"", preferredJobLocation:"", preferredJobType:"", shiftBasis:"", wearUniform:"", timestamp:"", randomNumber:"", guid:"", deviceID:"")
    }
    
    init(firstName:String, lastName:String, nationality:String, dob:String, kholasatAlQaid:String, gender:String, emailAddress:String, mobileNumber:String, emirates:String, language1:String, language2:String, language3:String, educationalLevel:String, major:String, graduationYear:String, yearsOfExp:String, domain:String, domain1:String, domain2:String, other:String, preferredJobLocation:String, preferredJobType:String, shiftBasis:String, wearUniform:String, timestamp:String, randomNumber:String, guid:String, deviceID:String)
    {
        self.First_Name = firstName
        self.Last_Name = lastName
        self.Nationality = nationality
        self.DOB = dob
        self.Kholasat_Al_Qaid = kholasatAlQaid
        self.Gender = gender
        self.Email_Address = emailAddress
        self.Mobile_Number = mobileNumber
        self.Emirates = emirates
        self.Language1 = language1
        self.Language2 = language2
        self.Language3 = language3
        self.Educational_Level = educationalLevel
        self.Major = major
        self.Graduation_Year = graduationYear
        self.Years_Of_Exp = yearsOfExp
        self.Domain = domain
        self.Domain1 = domain1
        self.Domain2 = domain2
        self.Other = other
        self.Preferred_Job_Location = preferredJobLocation
        self.Preferred_Job_Type = preferredJobType
        self.ShiftBasis = shiftBasis
        self.WearUniform = wearUniform
        self.Timestamp = timestamp
        self.RandomNumber = randomNumber
        self.Guid = guid
        self.DeviceID = deviceID
    }
}
