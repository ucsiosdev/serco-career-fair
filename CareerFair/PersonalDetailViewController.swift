//
//  PersonalDetailViewController.swift
//  CareerFair
//
//  Created by Prashannajeet Prasann on 12/03/19.
//  Copyright © 2019 Prashannajeet Prasann. All rights reserved.
//

import UIKit



class PersonalDetailViewController: UIViewController, UITextFieldDelegate,CareerFairPopOverDelegate {
    
    
    
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var nationalityTextField: UITextField!
    @IBOutlet var dobTextField: UITextField!
    
    @IBOutlet var yesButton: UIButton!
    @IBOutlet var noButton: UIButton!
    @IBOutlet var maleButton: UIButton!
    @IBOutlet var femaleButton: UIButton!
    
    var selectedGender = ""
    var selectedFamilyBook = ""
    
    var selectedTitle = ""
    var descriptionArray :[String] = []
    var registrationArray = [RegistrationDetails]()
    open var popoverSize = CGSize(width: 500, height: 500)
    var gradientView:UIView = UIView()
    var registrationObject = RegistrationDetails()

    @IBOutlet var number1Label: UILabel!
    @IBOutlet var number2Label: UILabel!
    @IBOutlet var number3Label: UILabel!
    @IBOutlet var number4Label: UILabel!
    
    //Mark:- Life Cycle Method
    override func viewDidLoad() {
        
        readJson()
        nationalityTextField.text = "United Arab Emirates"
        number1Label.layer.cornerRadius = 15
        number1Label.layer.masksToBounds = true
        number2Label.layer.cornerRadius = 15
        number2Label.layer.masksToBounds = true
        number3Label.layer.cornerRadius = 15
        number3Label.layer.masksToBounds = true
        number4Label.layer.cornerRadius = 15
        number4Label.layer.masksToBounds = true
        
        firstNameTextField.text = registrationObject.First_Name
        lastNameTextField.text = registrationObject.Last_Name
        nationalityTextField.text = registrationObject.Nationality
        dobTextField.text = registrationObject.DOB
        if registrationObject.Gender == "Male"{
            maleButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
            femaleButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        }else if registrationObject.Gender == "Female"{
            maleButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
            femaleButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        }
        
        if registrationObject.Kholasat_Al_Qaid == "Yes" {
            yesButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
            noButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        }else if registrationObject.Kholasat_Al_Qaid == "No"{
            yesButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
            noButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        }
    }
    
    
    private func readJson() {
        let url = Bundle.main.url(forResource: "nationalities", withExtension: "json")!
        do {
            let jsonData = try Data(contentsOf: url)
            let json = try JSONSerialization.jsonObject(with: jsonData, options: [])

            if let object = json as? [String: Any] {
                let tempArray = object["ListNationalityContract"]
                let testArray = tempArray as! NSArray
                
                for i in 0..<testArray.count{
                    let dic = testArray .object(at: i) as! NSDictionary
                    self.descriptionArray.append(dic .value(forKey: "Description") as! String)
                }

            }
        }
        catch {
            print(error)
        }
    }
    
    @IBAction func maleButtonTapped(_ sender: Any) {
        selectedGender = "Male"
        maleButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        femaleButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
    }
    
    @IBAction func femaleButtonTapped(_ sender: Any) {
        selectedGender = "Female"
        maleButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        femaleButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
    }
    @IBAction func yesButtonTapped(_ sender: Any) {
        selectedFamilyBook = "Yes"
        yesButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
        noButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
    }
    @IBAction func noButtonTapped(_ sender: Any) {
        selectedFamilyBook = "No"
        yesButton.setImage(UIImage(named: "UnChecked_Logo"), for: .normal)
        noButton.setImage(UIImage(named: "Checked_Logo"), for: .normal)
    }
    @IBAction func nextButtonTapped(_ sender: Any) {
        
        if firstNameTextField.text?.count == 0 {
            showAlert(withTitle: "", message: "Please enter your First Name")
        }else if lastNameTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please enter your Last Name")
        }else if dobTextField.text?.count == 0{
            showAlert(withTitle: "", message: "Please select your Date of Birth")
        }else if selectedFamilyBook == ""{
            showAlert(withTitle: "", message: "Please select family book")
        }else if selectedGender == ""{
            showAlert(withTitle: "", message: "Please select gender")
        }
        else{
           
            registrationObject.First_Name = firstNameTextField.text!
            registrationObject.Last_Name = lastNameTextField.text!
            registrationObject.Nationality = nationalityTextField.text!
            registrationObject.DOB = dobTextField.text!
            registrationObject.Kholasat_Al_Qaid = selectedFamilyBook
            registrationObject.Gender = selectedGender
            let vc = OtherInformationViewController()
            vc.registrationObject = registrationObject
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    //MARK: - UITextField Methods
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == nationalityTextField
        {
            self.view .endEditing(true)
            selectedTitle = "Nationality"
            let popoverVC = PopOverViewController()
            popoverVC.delegate = self
            popoverVC.stringsArray = descriptionArray
            popoverVC.delegate=self
            popoverVC.titleText = selectedTitle
            popoverVC.currentPopOverSize=self.popoverSize
            let visitNavController = UINavigationController(rootViewController: popoverVC)
            visitNavController.preferredContentSize = self.popoverSize
            visitNavController.modalPresentationStyle = .formSheet
            visitNavController.navigationBar.isHidden = true
            self.showGradiedntViewForPopOver()
            self.present(visitNavController, animated: true, completion: nil)
         }
        if textField == dobTextField {
            
            let date = Date.init()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy" // change format as per needs
            dobTextField.text = formatter.string(from: date)
            
            // Create a DatePicker
            let datePicker = UIDatePicker() //Date picker
            datePicker.frame = CGRect(x: 0, y: 0, width: 320, height: 216)
            datePicker.datePickerMode = .date
            datePicker.maximumDate = Date.init()
            datePicker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
            let popoverView = UIView() //view
            popoverView.backgroundColor = UIColor.clear
            popoverView.addSubview(datePicker)
            let popoverViewController = UIViewController()
            popoverViewController.view = datePicker
            popoverViewController.view.frame = CGRect(x: 0, y: 0, width: 320, height: 216)
            popoverViewController.modalPresentationStyle = .popover
            popoverViewController.preferredContentSize = CGSize(width: 320, height: 216)
            popoverViewController.popoverPresentationController?.sourceView = dobTextField // source button
            popoverViewController.popoverPresentationController?.sourceRect = dobTextField.bounds // source button bounds
            present(popoverViewController, animated: true)
        }
        
        return false
    }
    @objc func dateChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let selectedDate: String = dateFormatter.string(from: sender.date)
        dobTextField.text = selectedDate
    }
    
    func didCancelPopOver() {
        hideGradientViewForPopOver()
    }
    
    func didSelectStringinPopOver(selectedString: String) {
        nationalityTextField.text = selectedString
        hideGradientViewForPopOver()
    }
    
    func showGradiedntViewForPopOver()
    {
        gradientView = UIView(frame: (self.navigationController?.view.bounds)!)
        gradientView.backgroundColor = UIColor(red: 44.0/255.0, green: 57.0/255.0, blue: 74.0/255.0, alpha: 1.0)
        gradientView.layer.opacity = 0.5
        self.navigationController?.view.addSubview(gradientView)
    }
    func hideGradientViewForPopOver()  {
        gradientView.removeFromSuperview()
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
}
